function varargout = CombineSelections(varargin)
% EXPORTPAMGUARD MATLAB code for ExportPamguard.fig
%      EXPORTPAMGUARD, by itself, creates a new EXPORTPAMGUARD or raises the existing
%      singleton*.
%
%      H = EXPORTPAMGUARD returns the handle to a new EXPORTPAMGUARD or the handle to
%      the existing singleton*.
%
%      EXPORTPAMGUARD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EXPORTPAMGUARD.M with the given input arguments.
%
%      EXPORTPAMGUARD('Property','Value',...) creates a new EXPORTPAMGUARD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ExportPamguard_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ExportPamguard_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ExportPamguard

% Last Modified by GUIDE v2.5 23-Jul-2019 13:22:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ExportPamguard_OpeningFcn, ...
                   'gui_OutputFcn',  @ExportPamguard_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before ExportPamguard is made visible.
function ExportPamguard_OpeningFcn(hObject, eventdata, handles, varargin)

% Choose default command line output for ExportPamguard
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ExportPamguard wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = ExportPamguard_OutputFcn(hObject, eventdata, handles) 

% Get default command line output from handles structure
varargout{1} = handles.output;

% ---
function inFiles_ButtonDownFcn(hObject, eventdata, handles)
    % Import Utilities
    import utilities.*

    %--
    % Find current path for input selection tables
    %--
    inFilesPath = char(handles.inFiles.UserData);
    if ~isdir(inFilesPath)
        inFilesPath = pwd;
    end

    %--
    % Select selection tables for input
    %--
    filterSpect = fullfile(inFilesPath, sprintf('*.%s','txt'));
    [inFiles, inFilesPath2] = uigetfile( ...
        filterSpect, ...
        'Select selection tables for input', ...
        'Multiselect', 'on');
    if isequal(inFiles, 0)
        return;
    end
    handles.inFiles.UserData = inFilesPath2;

    if ~iscell(inFiles)
        inFiles = {inFiles};
    end
    handles.inFiles.String = inFiles;

% ---
function criterion_Callback(hObject, eventdata, handles)
    t = get(hObject,'String');
    try
        n = str2double(t);
        if isnan(n) || isinf(n) || n<0
            n = 1;
        end
    catch
        n = 1;
    end
    set(hObject,'String',n);

% ---
function criterion_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
  
% ---
function outpath_ButtonDownFcn(hObject, eventdata, handles)

    %--
    % Find current path
    %--
    outpath = char(handles.outpath.UserData);
    if ~isfolder(outpath)
        outpath = pwd;
    end

    %--
    % Select new input path
    %--
    outpath = uigetdir(outpath, 'Select folder for output');
    if isequal(outpath, 0)
        return;
    end
    handles.outpath.UserData = outpath;
    handles.outpathTitle.TooltipString = outpath;
    [~, outpathDisplay] = fileparts(outpath);
    handles.outpath.String = outpathDisplay;
    
% ---
function runButton_Callback(hObject, eventdata, handles)

    %Initializations
    import util.*
    inFiles = handles.inFiles.String;
    inFilesPath = handles.inFiles.UserData;
    inFilesFull = fullfile(inFilesPath,inFiles);
    crit = str2double(handles.criterion.String);
    outpath = handles.outpath.UserData;
    
    %Test inputs
    assert(~isempty(inFiles),...
        sprintf('\n\n  *** No input selection tables selected! ***\n'))
    assert(all(isfile(inFilesFull)),...
        sprintf('\n\n  *** At least one of the input files does not exist! ***\n'))
    assert(isfolder(outpath),...
        sprintf('\n\n  *** Output folder does not exist! ***\n'))
    
    % For each input selection table
    for i = 1:length(inFiles)
    
        % Read selection table
        C = read_selection_table(inFilesFull{i});
        
        % Test that selection table has at least two selections
        assert(size(C,1) > 3,...
            sprintf('\n\n  *** Selection table has fewer than two selections:\n  %s ***\n',inFilesFull{i}))
        
        % Identify selections to be combined
        beginTime = str2double(get_field(C,'Begin Time (s)'));
        endTime = str2double(get_field(C,'End Time (s)'));
        dt = diff([endTime(1:end-1),beginTime(2:end)],1,2);
        keep = dt >= crit;
        
        % Combine selections
        beginTime2 = strtrim(cellstr(num2str(beginTime([true ; keep]))));
        endTime2 = strtrim(cellstr(num2str(endTime([keep ; true]))));
        
        % Find channel and  frequency bounds of first selection in series
        chan = get_field(C,'Channel');
        chan2 = chan([true ; keep]);
        lowFreq = get_field(C,'Low Freq (Hz)');
        lowFreq2 = lowFreq([true ; keep]);
        highFreq = get_field(C,'High Freq (Hz)');
        highFreq2 = highFreq([true ; keep]);
    
        % Set up selection ID's
        id = strtrim(cellstr(num2str((1:size(beginTime2,1))')));
        
        % Set up View column
        view(1:size(beginTime2,1),1) = {'Spectrogram 1'};
        
        % Make body of selection table array
        C2 = [id,view,chan2,beginTime2,endTime2,lowFreq2,highFreq2];
        
        % Add header line
            header = {'Selection', ...
              'View', ...
              'Channel', ...
              'Begin Time (s)', ...
              'End Time (s)', ...
              'Low Freq (Hz)', ...
              'High Freq (Hz)', ...
            };
        C2 = [header ; C2];
        
        % Write selection table
        [~,inFile] = fileparts(inFilesFull{i});
        inFile = strrep(inFile,'.selections.','');
        outFile = sprintf('%s.combined_%fs_%s.selections.txt',inFile,crit,datestr(now,'yyyymmdd_HHMMSS'));
        outfileFull = fullfile(outpath,outFile);
        write_selection_table(C2,outfileFull)
        
    end

    disp(' ')
    disp('****************************************************************************')
    fprintf('Finished writing: %s\n',outfileFull);
    disp('****************************************************************************')
