function [C2,idx] = get_field(C, header)
%
% Description:
%   Returns the values in one column of a Raven selection table, as
%   specified by column header.
%
% Usage:
%   get_field
%
% Inputs:
%   C - selection table, including headers (cell array of char vectors)
%   header - header for column of interest (char vectors)
%
% Outputs:
%   C - column values cell array of char vectors)
%   idx - index of selected header in first row of selection table
%
% History
%   msp2  Aug-15-2017   Initial

% if there are no selections, return empty cell string
if size(C,1)<2
    C={''};
end

% Find index of column with specified header
headers = C(1, :);
idx = ismember(headers, header);
idx = find(idx, 1); % if multiple columns, choose first

% Copy values in specified column
C2 = C(:, idx);
C2 = C2(2:end, :);